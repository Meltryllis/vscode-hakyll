# Change Log

All notable changes to the "vscode-hakyll" extension will be documented in this file.

## [Unreleased]

- Nothing

## [0.0.3]

- Able to generate and deploy together

## [0.0.2]

- Deploy note changed

## [0.0.1]

- Initial release
